<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */
get_header('conference'); ?>
    <nav class="level box  is-paddingless is-mobile conferences-page has-background-white-ter">
        <a class="level-item is-hidden-small-mobile has-background-black">
            <figure class="image is-180x180-tablet is-128x128-mobile">
                <img src="https://hashtagcauseascene.com/wp-content/uploads/2018/08/causeascene-conf-stacked-sq-outs.png" alt="#causeascene conf logo">
            </figure>
        </a>
        <a class="level-item is-hidden-big-mobile has-background-black">
            <figure class="image is-64x64">
                <img src="https://hashtagcauseascene.com/wp-content/uploads/2018/08/causeascene-conf-stacked-sq-outs.png" alt="#causeascene conf logo">
            </figure>
        </a>
        <a href="#conf-speakers" class="level-item">
            <p class="title  is-size-3-desktop is-size-4-tablet  is-size-7-mobile has-text-black">Speakers</p>
        </a>
        <a href="#conf-partners" class="level-item">
            <p class="title  is-size-3-desktop is-size-4-tablet is-size-7-mobile has-text-black">Partners</p>
        </a>
        <p class="level-item">
            <a href="<?php echo get_post_meta(get_the_ID(), 'ticketing_site', true)?>" class="button is-medium is-green  is-outlined has-text-weight-bold is-size-3-desktop is-size-4-tablet is-size-7-mobile"
                target="_blank">
                <b>Get Tickets!</b>
            </a>

        </p>
    </nav>

    <div class="container is-fluid conferences-page">


        <section class="section conf-heading">

            <h1 class="is-size-huge-tablet is-size-1-mobile  main-title has-text-black has-text-weight-extra-bold">
                <p>
                    <?php the_title(); ?>

                </p>
            </h1>
        </section>
        <section class="section conf-details">

            <?php if(!get_field("job_fair")){ ?>
            <h5 class="conf-details-heading ">

                <p class="has-text-black has-text-weight-bold">
                    <?php 
                $date =  date_create_from_format ('Ymd' , get_field("date"));
                echo date_format($date,"F j, Y"); 
                ?>
                </p>
                <p>Conference
                    <?php the_field("time") ?>
                </p>

            </h5>
            <?php } else { ?>
            <?php if(!get_field("different_date")){ ?>
            <h5 class="conf-details-heading ">

                <p class="has-text-black has-text-weight-bold">
                    <?php 
                $date =  date_create_from_format ('Ymd' , get_field("date"));
                echo date_format($date,"F j, Y"); 
                ?>
                </p>

                <div class="columns is-mobile is-multiline">
                    <div class="column is-5-mobile is-4-tablet has-text-black has-text-weight-semi-bold">
                        Career Coaching Fair
                    </div>
                    <div class="column is-7-mobile is-8-tablet">
                        <?php the_field("job_fair_time") ?>
                    </div>
                    <div class="column is-5-mobile is-4-tablet has-text-black has-text-weight-semi-bold">
                        Conference
                    </div>
                    <div class="column is-7-mobile is-8-tablet ">
                        <?php the_field("time") ?>
                    </div>
                </div>
            </h5>

    <?php } else { ?>
    <h5 class="conf-details-heading ">
        <p class="has-text-black has-text-weight-bold">
            <?php 
                $date =  date_create_from_format ('Ymd' , get_field("job_fair_date"));
                echo date_format($date,"F j, Y"); 
                ?> 
        </p>

        <p class="detail-margin"> Career Coaching Fair <br></p>

        <p class="detail-margin">
            <?php the_field("job_fair_time") ?><br>
        </p>

        <?php if(get_field("different_location")){?>
        <p class="is-italic detail-margin">
            <?php the_field("job_fair_location") ?>
        </p>
        <p class="is-italic">
            <?php the_field("job_fair_address") ?>
        </p>
        </h5>

        <?php } ?>

        <hr>
        <h5 class="conf-details-heading ">
            <p class="has-text-black has-text-weight-bold ">
                <?php 
                $date =  date_create_from_format ('Ymd' , get_field("date"));
                echo date_format($date,"F j, Y"); 
                ?>
            </p>

            <p class="detail-margin"> Conference<br</p>
            <p class="detail-margin"><?php the_field("time") ?></p>
          
            <?php if(get_field("different_location")){?>
            <p class = "is-italic detail-margin">
                
                    <?php the_field("location_name") ?>
               
            </p>
            <p class = "is-italic">
            
                    <?php the_field("address") ?> 
            </p>
            </h5>

            <?php } ?>
            
            <?php } ?>

            <?php } ?>

            <?php if(!get_field("different_location")){?>
            <hr>
            <h5 class="conf-details-heading">
                <p class="is-italic">
                    <?php the_field("location_name") ?>
                </p>
                <p class="is-italic">
                    <?php the_field("address") ?>
                </p>
            </h5>
            <?php } ?>



            </section>

            <section id="conf-speakers" class="section speakers">
                <h3>
                    <strong>Speakers</strong>
                </h3>
                <?php
                    if( have_rows('speakers') ){
                        while ( have_rows('speakers') ) : the_row();
                ?>
                    <article class="media">
                        <div class="media-left is-hidden-mobile">

                            <figure class="image is-64x64-mobile is-128x128-tablet  has-text-centered">
                                <?php 
                            $image = get_sub_field('speaker_image');
                            if($image){ ?>

                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" style="margin:auto;" />

                                <?php } else {?>
                                <img src="https://hashtagcauseascene.com/wp-content/uploads/2018/08/causeascene-soloHash-outlines.jpg" alt="default avatar"
                                    style="margin:auto;" />
                                <?php } ?>
                            </figure>

                        </div>
                        <div class="media-content">
                            <div class="content">
                                <figure class="image is-128x128 is-inline-block has-text-centered is-hidden-tablet" style="margin-left:0px; margin-bottom:0px;">
                                    <?php 
                            $image = get_sub_field('speaker_image');
                            if($image){ ?>

                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" style="margin:auto;" />

                                    <?php } else {?>
                                    <img src="https://hashtagcauseascene.com/wp-content/uploads/2018/08/causeascene-soloHash-outlines.jpg" alt="default avatar"
                                        style="margin:auto;" />
                                    <?php } ?>
                                </figure>
                                <p>
                                    <strong class="is-size-4-tablet is-size-5-mobile">
                                        <?php the_sub_field('speaker_name'); ?>

                                        <?php $twitter = get_sub_field('twitter_profile_url');
                                    if($twitter){ ?>

                                        <a href="<?php echo $twitter ?>" target="_blank" class="icon">
                                            <i class="fab fa-twitter"></i>
                                        </a>

                                        <?php } 
                                    $website = get_sub_field('website');
                                    if($website ){ ?>

                                        <a href="<?php echo $website ?>" target="_blank" class="icon">
                                            <i class="fas fa-globe"></i>
                                        </a>
                                        <?php } ?>

                                    </strong>
                                    <br/>
                                    <strong class="is-size-4-tablet is-size-5-mobile is-text-pink">
                                        <?php the_sub_field('talk_title'); ?>
                                        <?php 
                                    $talk = get_sub_field('talk_link');
                                    if($talk){ ?>

                                        <a href="<?php echo  $talk  ?>" target="_blank" class="icon has-text-danger">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                        <?php } ?>

                                    </strong>
                                    <?php 
                                $abstract = get_sub_field('talk_abstract');
                                if($abstract){ ?>
                                    <br/>
                                    <span class="format-break-paragraph is-size-5-tablet is-size-6-mobile has-text-weight-semibold">
                                        <?php echo $abstract ?>
                                    </span>
                                    <?php } ?>
                                    <br/>
                                </p>
                            </div>
                            <?php 
                            $bio = get_sub_field('speaker_bio');
                            if($bio){ ?>
                            <article class="media">
                                <div class="media-content">
                                    <div class="content">
                                        <p>
                                            <span class="is-size-6-tablet is-size-7-mobile is-text-green">About
                                                <?php the_sub_field('speaker_name'); ?> </span>
                                            <br>
                                            <span class="format-break-paragraph is-size-6-tablet is-size-7-mobile">
                                                <?php echo $bio ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>

                            </article>

                            <?php } ?>

                        </div>

                    </article>

                    <?php   endwhile;
                    } else {
                        echo "<p>";
                        echo "TBA!";
                        echo "</p>";
                    }
                ?>
                    <?php
                    $cfpOpen = get_post_meta(get_the_ID(), 'cfp_open', true);
                    $cfp = get_post_meta(get_the_ID(), 'cfp_form', true);
                    if($cfpOpen && $cfp){
                ?>
                        <br>
                        <a href="<?php echo $cfp ?>" target="_blank" class="button is-medium is-green  is-outlined has-text-weight-bold is-size-2-tablet is-size-4-mobile">Become a Speaker!</a>
                        <?php   }?>
            </section>

            <section id="conf-partners" class="section partners">

                <h3>
                    <strong>Partners</strong>
                </h3>



                <?php
                    if( have_rows('partners') ){ ?>
                    <div class="columns is-multiline">

                        <?php while ( have_rows('partners') ) : the_row();
                ?>
                        <div class="column is-4 has-text-centered">

                            <a class="image is-vertical-align-when-not-mobile is-inline-block" href="<?php echo  get_sub_field('url_of_partner') ?>"
                                alt="<?php echo  get_sub_field('name') ?>" target="_blank">
                                <img src="<?php echo  get_sub_field('partner_logo') ?>" alt="<?php echo  get_sub_field('name') ?>">
                            </a>

                        </div>
                        <?php   endwhile; ?>

                    </div>
                    <?php }  else {
                        echo "<p>";
                        echo "TBA!";
                        echo "</p>";
                    }
                ?>
                    <?php 
            if(get_field('partner_prospectus')){ ?>

                    <a href="<?php the_field('partner_prospectus'); ?>" target="_blank" class="button is-medium is-green  is-outlined has-text-weight-bold is-size-2-tablet is-size-4-mobile">Become a Partner!</a>

                    <?php } ?>

            </section>


            </div>

            <nav class="level box is-mobile conferences-page has-background-white-ter" style="margin-bottom: 0px;">
            <p class="level-item">
                    <a href="https://hashtagcauseascene.com/code-of-conduct/" class="button is-medium is-green  is-outlined has-text-weight-bold is-size-3-desktop is-size-4-tablet is-size-7-mobile"
                        target="_blank">
                        <b>Code of Conduct</b>
                    </a>
                </p>
                <p class="level-item is-size-3-desktop is-size-4-tablet is-size-7-mobile is-marginless">
                    <span class="has-text-weight-bold has-text-black ">#causeascene</span>&nbsp;
                    <span class="has-text-weight-bold has-text-grey-light">|</span>&nbsp;
                    <span class="has-text-weight-extra-bold has-text-black">CONF</span>
                </p>
    
            </nav>

            <?php get_footer('conference'); ?>