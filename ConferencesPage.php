<?php /* Template Name: ConferencesPage */ ?>

<?php get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main conferences-page " role="main">

    <h3>
      <p class="is-size-big-tablet is-size-3-mobile is-hidden-small-mobile is-marginless">
        <span class="has-text-weight-bold has-text-black ">#causeascene</span>&nbsp;<span class="has-text-weight-bold has-text-grey-light">|</span>&nbsp;<span class="has-text-weight-extra-bold has-text-black">CONF</span>
      </p>
      <p class="is-size-5-mobile is-hidden-big-mobile is-marginless">
        <span class="has-text-weight-bold has-text-black">#causeascene</span>&nbsp;<span class="has-text-weight-bold has-text-grey-light">|</span>&nbsp;<span class="has-text-weight-extra-bold has-text-black">CONF</span>
      </p>
      <p class="is-size-6-mobile has-text-weight-bold has-text-black">The&nbsp;Human&nbsp;Side Of&nbsp;Tech</p>
    </h3>

    <h3>
      <b>About</b>
    </h3>
    <?php the_field('about') ?>

    <div style="text-align:center;">

      <a href="https://hashtagcauseascene.com/code-of-conduct/">
        <button>Code of Conduct</button>
      </a>

    </div>


    <h3>
      <b>Tour</b>
    </h3>

    <div class="columns is-multiline">

      <?php

      $args = array(
        'post_type' => 'event',
        'orderby'   => 'meta_value_num',
        'meta_key'  => 'date',
        'order'     => 'ASC'
      );



      $products = new WP_Query( $args );
      if( $products->have_posts() ) {
        while( $products->have_posts() ) {
          $products->the_post();
          ?>


        <?php if(get_post_meta(get_the_ID(), 'active', true)){ ?>
        <div class="column is-4-tablet is-10-mobile  is-offset-1-mobile is-flex">
          <a class="conf-info box" href="<?php echo get_permalink(get_the_ID()); ?>" target="_blank">
            <p class="has-text-weight-extra-bold has-text-black">
            
                <?php the_title(); ?>
             
            </p>
            <p >
              <?php 
              $date =  date_create_from_format ('Ymd' , get_post_meta(get_the_ID(), 'date', true));
              echo date_format($date,"F j, Y");
              ?>
            </p>
          </a>
        </div>

        <?php } ?>


        <?php
        }
      } else {
          echo 'No conferences!';
        }
  ?>
    </div>

    <h3>
      <b>Past Events</b>
    </h3>

    <div class="columns is-multiline">

      <?php

      $args = array(
        'post_type' => 'event',
        'orderby'   => 'meta_value_num',
        'meta_key'  => 'date',
        'order'     => 'DESC'
      );



      $products = new WP_Query( $args );
      if( $products->have_posts() ) {
        while( $products->have_posts() ) {
          $products->the_post();
          ?>


        <?php if(!get_post_meta(get_the_ID(), 'active', true)){ ?>
        <div class="column is-4-tablet is-10-mobile  is-offset-1-mobile is-flex">
          <a class="conf-info box" href="<?php echo get_permalink(get_the_ID()); ?>" target="_blank">
            <p class="has-text-weight-extra-bold has-text-black">
             
                <?php the_title(); ?>
            
            </p>
            <p class="has-text-weight-extra-normal">
              <?php 
                    $date =  date_create_from_format ('Ymd' , get_post_meta(get_the_ID(), 'date', true));
                    echo date_format($date,"F j, Y");
                    ?>
            </p>
          </a>
        </div>

        <?php } ?>


        <?php
      }
        } else {
              echo 'No conferences!';
          }
          ?>
    </div>
    <p id="ConferenceContact">
      Please contact Kim is you have any questions or if you would like your city to be the next spot on the <span class="has-text-weight-bold has-text-black">#causeascene <span class="has-text-weight-extra-bold">CONF</span></span> Tour.
    </p>
    <div style="text-align:center;">
      <a href=" https://hashtagcauseascene.com/contact/ ">
        <button>Contact Kim</button>
      </a>
    </div>


  </main>
  <!-- #main -->

</div>
<!-- #primary -->

<?php
do_action( 'storefront_sidebar' );
get_footer();