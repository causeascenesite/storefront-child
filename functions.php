<?php


function theme_styles() {
   
    wp_enqueue_style( 'bulma-css', get_stylesheet_directory_uri().'/bulma.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array(), time() );
    wp_enqueue_style( 'm-font-style', 'https://fonts.googleapis.com/css?family=Montserrat:bold,300,400,900' );

}


add_action( 'wp_enqueue_scripts', 'theme_styles' );

function wpcustom_deregister_scripts_and_styles(){
    if(is_singular('event')) {
      
        wp_deregister_style('storefront-woocommerce-style');
        wp_deregister_style('storefront-style');

        wp_dequeue_style('storefront-woocommerce-style');
        wp_dequeue_style('storefront-style');

      }
}
add_action( 'wp_print_styles', 'wpcustom_deregister_scripts_and_styles', 100 );

// add_action('woocommerce_shipping_free_shipping_is_available', '__return_true');